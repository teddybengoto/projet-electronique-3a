/***************************************************************************
* Nom du prg 	:  
* Description	: 
*
* Auteur	: 
* Cr�� le	:
* Compilateur	: 
***************************************************************************
* Modifi� le  	:
***************************************************************************/


#include <pic18f26k22.h>
#include "lib_demo.h"
#include "stdio.h"
#include <xc.h>
#include "string.h"

#define _XTAL_FREQ 16000000
//#define _XTAL_FREQ 8000000

/*----------------------------------------------------------------------------*/
/*Ins�rer Ici les bits de configuration pour le �C 							  */
/* -> Copier / Coller depuis le fichier Config.txt							  */
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
//  18F26K22
/*----------------------------------------------------------------------------*/
                                 
/* Voir fichier  pic18_chipinfo.html pour conna�tre les bits de config de la cible*/
#pragma	config PLLCFG = OFF  	// 4X PLL Enable (ON/OFF)
#pragma config PRICLKEN = ON	// PRICLKEN =	Primary clock enable bit (OFF	Primary clock can be disabled by software, ON	Primary clock is always enabled)
#pragma config FCMEN = OFF  // Fail-Safe Clock Monitor Enable bit
#pragma config IESO = OFF   // Internal/External Oscillator Switchover bit (ON / OFF)
#pragma config FOSC = INTIO7     // Oscillator Selection bits (XT, HS, ...)

#pragma config BOREN = OFF  // Brown-out Reset Enable bits (ON, OFF, NOSLP, SBORDIS)
#pragma config BORV =  220    // Brown Out Reset Voltage bits
#pragma config PWRTEN = ON    // Power-up Timer Enable bit (ON / OFF)

#pragma config WDTPS = 256  // Watchdog Timer Postscale Select bits
#pragma config WDTEN = OFF    // Watchdog Timer Enable bit

#pragma config CCP2MX = PORTC1   // CCP2 MUX bit (PORTC1, PORTB3)
#pragma config CCP3MX = PORTC6   // CCP3 MUX bit (PORTB5, PORTC6)
#pragma config T3CMX = PORTC0	// T3CMX =	Timer3 Clock input mux bit (PORTC0/PORTB5)
#pragma config HFOFST = OFF		// HFINTOSC Fast Start-up
#pragma config PBADEN =	OFF		//	PORTB A/D Enable bit
#pragma config MCLRE = EXTMCLR   // MCLR Pin Enable bit
#pragma config P2BMX = PORTB5  // ECCP2 B output mux bit (PORTB5/PORTC0)

#pragma config STVREN = OFF // Stack Full/Underflow Reset Enable bit
#pragma config DEBUG = OFF  // Background Debugger Enable bit
#pragma config LVP = OFF    // Single-Supply ICSP Enable bit
#pragma config XINST = OFF  // Extended Instruction Set Enable bit

// Protection lecture
#pragma config CP0 = OFF, CP1 = OFF, CP2 = OFF, CP3 = OFF
#pragma config CPD = OFF, CPB = OFF
#pragma config WRT0 = OFF, WRT1 = OFF, WRT2 = OFF, WRT3 = OFF
#pragma config WRTB = OFF, WRTC = OFF, WRTD = OFF
#pragma config EBTR0 = OFF, EBTR1 = OFF, EBTR2 = OFF, EBTR3 = OFF
#pragma config EBTRB = OFF
/*----------------------------------------------------------------------------*/
/* D�clarations des variables globales 	*/
uint8_t CarRec = 0;
char MemoCar[1000]={0};


/* Enregistrement trame */

/*
    
void savetrame(uint8_t data)
{

     if (data=='0x2') { // EOF ETX
        
        MemoCar =  CarRec << 8*skip ;
        skip ++;
    }
 }*/



/* Directives de compilation		*/
					

/* Interruptions haute priorit�		*/
void __interrupt(high_priority) ItHigh(void)
{

    if (PIR1bits.RC1IF && PIE1bits.RC1IE)       // Discrimination IRQ
    {
        CarRec = RCREG1;                        // R�cup�ration octet re�u
        PIR1bits.RC1IF = 0;                     // RAZ Flag
    }
}

/* Interruptions Basse priorit�	*/
void __interrupt(low_priority) ItLow(void)
{

}


/* Programme Principal			*/
void main(void)
{


Initialiser();
int st = 0;
int i = 500;
int j;
    
while(1)
    {
    
     LATBbits.LATB1  = 1;    // Toggle LED sur RB5

    if (CarRec != 0)        // SI un caract�re a �t� re�u
        {
        LATBbits.LATB5  = ~LATBbits.LATB5;    // Toggle LED sur RB5
        MemoCar[st] = CarRec;//CarRec;
        st++;
       // if(CarRec=='EOT') //SOF == start Of Tram and End Of Frame (EOF) : 0xAA
        {
            
            LATBbits.LATB0 = ~LATBbits.LATB0;   // Toggle LED sur RB0
            //MemoCar[st] = CarRec;//CarRec;
            //st++;
        
            if(st==i) 
        {
             for (j = 0 ; j < i ; j++)
                {
                     MemoCar[j]=0;
                }
              st= 0;

        }

        }
          CarRec = 0;             // RAZ flag logiciel
          
        }
        
       

    }
}					


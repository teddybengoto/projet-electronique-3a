/***************************************************************************
* Nom du prg 	:  
* Description	: 
*
* Auteur	: 
* Cr�� le	:
* Compilateur	: 
***************************************************************************
* Modifi� le  	:
***************************************************************************/


#include <pic18f26k22.h>

#include "lib_demo.h"

/* D�clarations des variables globales 	*/

/* Directives de compilation		*/
					

/*	Impl�mentation du code */
void Initialiser(void)
{
// Configuration oscillateur interne � 8MHz
//OSCCON = 0x60;
OSCCON = 0x60;    
    
//OSCCON = 0b1111000;    
//Config RBx
TRISBbits.TRISB0 = 0;   // RB0 en sortie (LED)
TRISBbits.TRISB1 = 0;   // RB1 en sortie (LED
TRISBbits.TRISB5 = 0;   // RB5 en sortie (LED)

LATBbits.LATB0 = 0;     // LED off au d�marrage
LATBbits.LATB5 = 0;     // LED off au d�marrage

// Configuration UART1 (9600bps @8MHz)        

TRISCbits.TRISC7 = 1;   // Ligne RXD en entr�e
ANSELCbits.ANSC7 = 0;   // Activation buffer # 
TRISCbits.TRISC6 = 0;   // Ligne TXD en sortie

TXSTA1 = 0x24;
RCSTA1 = 0x90;
SPBRG1 = 52;            //


// � convertire 115200 

//52;    // 9600bps (valeur th�orique = 51) 


// Configuration IRQ (g�n�ral))
RCONbits.IPEN = 1;      // Activation Priorit�s
INTCONbits.GIEH = 1;    // IRQ haute priorit� activ�es
INTCONbits.GIEL = 1;    // IRQ basse priorit� activ�es

// Config IRQ RX UART1
IPR1bits.RC1IP = 1;     // IRQ UART1 Rx en haute priorit�
PIR1bits.RC1IF = 0;     // Mise � 0 du flag (s�curit�)    
PIE1bits.RC1IE = 1;     // Autorisation IRQ UART1 RX



}


void    EmettreCar(uint8_t Car)
{
    while(!PIR1bits.TX1IF); // Attente que le buffer Tx soit libre
    TXREG1=Car;             // Emission caract�re
}
					
					
					

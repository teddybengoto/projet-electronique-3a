/***************************************************************************
* Nom du prg 	:  
* Description	: 
*
* Auteur	: 
* Cr�� le	:
* Compilateur	: 
***************************************************************************
* Modifi� le  	:
***************************************************************************/


#include "lib_demo_pwm.h"

/*----------------------------------------------------------------------------*/
/*Ins�rer Ici les bits de configuration pour le �C 							  */
/* -> Copier / Coller depuis le fichier Config.txt							  */
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
//  18F26K22
/*----------------------------------------------------------------------------*/
                                 
/* Voir fichier  pic18_chipinfo.html pour conna�tre les bits de config de la cible*/
#pragma	config PLLCFG = OFF  	// 4X PLL Enable (ON/OFF)
#pragma config PRICLKEN = ON	// PRICLKEN =	Primary clock enable bit (OFF	Primary clock can be disabled by software, ON	Primary clock is always enabled)
#pragma config FCMEN = OFF  // Fail-Safe Clock Monitor Enable bit
#pragma config IESO = OFF   // Internal/External Oscillator Switchover bit (ON / OFF)
#pragma config FOSC = INTIO7     // Oscillator Selection bits (XT, HS, ...)

#pragma config BOREN = OFF  // Brown-out Reset Enable bits (ON, OFF, NOSLP, SBORDIS)
#pragma config BORV =  220    // Brown Out Reset Voltage bits
#pragma config PWRTEN = ON    // Power-up Timer Enable bit (ON / OFF)

#pragma config WDTPS = 256  // Watchdog Timer Postscale Select bits
#pragma config WDTEN = OFF    // Watchdog Timer Enable bit

#pragma config CCP2MX = PORTC1   // CCP2 MUX bit (PORTC1, PORTB3)
#pragma config CCP3MX = PORTC6   // CCP3 MUX bit (PORTB5, PORTC6)
#pragma config T3CMX = PORTC0	// T3CMX =	Timer3 Clock input mux bit (PORTC0/PORTB5)
#pragma config HFOFST = OFF		// HFINTOSC Fast Start-up
#pragma config PBADEN =	OFF		//	PORTB A/D Enable bit
#pragma config MCLRE = EXTMCLR   // MCLR Pin Enable bit
#pragma config P2BMX = PORTB5  // ECCP2 B output mux bit (PORTB5/PORTC0)

#pragma config STVREN = OFF // Stack Full/Underflow Reset Enable bit
#pragma config DEBUG = OFF  // Background Debugger Enable bit
#pragma config LVP = OFF    // Single-Supply ICSP Enable bit
#pragma config XINST = OFF  // Extended Instruction Set Enable bit

// Protection lecture
#pragma config CP0 = OFF, CP1 = OFF, CP2 = OFF, CP3 = OFF
#pragma config CPD = OFF, CPB = OFF
#pragma config WRT0 = OFF, WRT1 = OFF, WRT2 = OFF, WRT3 = OFF
#pragma config WRTB = OFF, WRTC = OFF, WRTD = OFF
#pragma config EBTR0 = OFF, EBTR1 = OFF, EBTR2 = OFF, EBTR3 = OFF
#pragma config EBTRB = OFF
/*----------------------------------------------------------------------------*/
/* D�clarations des variables globales 	*/



/* Directives de compilation		*/
					

/* Interruptions haute priorit�		*/
void __interrupt(high_priority) ItHigh(void)
{

}

/* Interruptions Basse priorit�	*/
void __interrupt(low_priority) ItLow(void)
{

}

/* Programme Principal			*/

void main(void)
{
uint8_t ton = 0;    // Valeur de ton en n/PR2 (n min = 0 : alpha = 0%, n max = 124 : alpha = 100%)

Initialiser();

while(1)
    {
    CCPR1L = ton;   // Affectation de la valeur de ton -> application nouveau rapport cyclique
    
    if (ton < 124) ton++;
    else ton = 0;
    
    __delay_ms(50); // D�lai de 50ms avant de reprendre la boucle
    
    }
}					

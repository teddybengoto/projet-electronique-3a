/***************************************************************************
* Nom du prg 	:  
* Description	: 
*
* Auteur	: 
* Cr�� le	:
* Compilateur	: 
***************************************************************************
* Modifi� le  	:
***************************************************************************/


#include <pic18f26k22.h>

#include "lib_demo_pwm.h"

/* D�clarations des variables globales 	*/

/* Directives de compilation		*/
					

/*	Impl�mentation du code */
void Initialiser(void)
{
// Configuration oscillateur interne � 8MHz
OSCCON = 0x60;   

//------------------------------------------------------------------------------
// Configuration PWM

// Ligne RC2/CCP1 configur�e en sortie (c'est sur cette broche que le signal PWM sera g�n�r�)
TRISCbits.TRISC2 = 0;

// TMR2 utilis� pour fixer la fr�quence de la PWM (ici Fpwm = 1kHz)
T2CON = 0b00000010; // Prescaler = 16 (Tclkbase = 8�s)
PR2 = 124;          // 125*8�s = 1ms (cf p187 de la doc, equation 14-1)

// Module CCP1 en PWM standard - config de d�part � Alpha = 50%
CCP1CON = 0x0C;     // Mode PWM

CCPTMRS0 = 0x00;    // Timer2 utilis� par le module CCP1
CCPTMRS1 = 0x00;    // idem (suite)

CCPR1L = 62;        // Pour fixer le rapport cyclique de d�part � 50% (alpha = CCPR1L / PR2)
//------------------------------------------------------------------------------

T2CONbits.TMR2ON = 1;   // d�marrage timer 2

}

					
					